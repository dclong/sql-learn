--------------------------------------------------Combine Raw Table--------------------------------------------------
--Convert to MPA naming convention
If Object_id('tempdb.dbo.#temp0') is not null drop table #temp0
CREATE TABLE #temp0
(
LoanID varchar(255)
,YYYY_MM varchar(255)
,Property varchar(255)
,Occupancy varchar(255)
,Purpose varchar(255)
,RateType varchar(255)
,FixedRatePeriod varchar(255)
,LienPosition varchar(255)
,Documentation varchar(255)
,OriginalAppraisalAmount varchar(255)
,LTV varchar(255)
,JrLTV varchar(255)
,CombinedLTV varchar(255)
,OriginalAmount varchar(255)
,Srbalance varchar(255)
,OriginalTerm varchar(255)
,AmorTerm varchar(255)
,IOTerm varchar(255)
,State varchar(255)
,Zip varchar(255)
,FICO varchar(255)
,UpdatedFICO varchar(255)
,Rate varchar(255)
,OriginationDate varchar(255)
,PrepayPenaltyTerm varchar(255)
,MaxDrawnAmount varchar(255)
,DrawdownTerm varchar(255)
,DTI varchar(255)
,CurrentAmount varchar(255)
,ReportedLoanStatus varchar(255)
,RealizedLoss varchar(255)
,FINAL_LGD_1  varchar(255)
,IsModified varchar(255)
,IsBroker varchar(255)
,IsHeloc varchar(255)
,IsHarris varchar(255)
,IsDefault varchar(255)
,IsPrepaid varchar(255)
,c_interest_only varchar(255)
,ORIG_INT_RT varchar(255)
,spec_pgm_id varchar(255)
,modif_dt varchar(255)
,cntrct_cd varchar(255)
)

insert into #temp0    
select 
LoanID = SK_ACCT_ID
,YYYY_MM
,Property = PROP_TYP_CD
,Occupancy = PROP_OCC_CD 
,Purpose = LN_PRPS_CD 
,RateType = ARM 
,FixedRatePeriod = NULL
,LienPosition = 1 
,Documentation = c_prod_typ_ds 
,OriginalAppraisalAmount = COLL_AMT
,LTV = ORIG_LTV_RTO 
,JrLTV = 0 
,CombinedLTV = ORIG_LTV_RTO
,OriginalAmount = LN_AMT 
,Srbalance = 0 
,OriginalTerm = TERM
,AmorTerm = NULL 
,IOTerm = NULL 
,State = STATE
,Zip = 	zip
,FICO = ORIG_CRB_SCR
,UpdatedFICO = CUR_CRB_SCR
,Rate = CUR_INT_RT
,OriginationDate = LN_ORIG_DT
,PrepayPenaltyTerm = NULL
,MaxDrawnAmount = NULL 
,DrawdownTerm = NULL 
,DTI = TOT_DTI_RTO
,CurrentAmount = CUR_BAL_AMT
,ReportedLoanStatus = reportedloanstatus
,RealizedLoss = NULL
,FINAL_LGD_1  
,IsModified = NULL
,IsBroker = NULL
,IsHeloc = 0 
,IsHarris = 1 
,IsDefault = case when reportedloanstatus in ('defaulted','Write Off')  then 1 else 0 end
,IsPrepaid = case when prepay = 'Y' then 1 else 0 end
,c_interest_only 
,ORIG_INT_RT 
,spec_pgm_id 
,modif_dt = null 
,cntrct_cd
from [ResearchDev].dbo.BMO_HARRIS_MTG


insert into #temp0    
select 
LoanID = SK_ACCT_ID
,YYYY_MM
,Property = PROP_TYP_CD
,Occupancy = PROP_OCC_CD 
,Purpose = LN_PRPS_CD 
,RateType = ARM 
,FixedRatePeriod = NULL
,LienPosition = 1 
,Documentation = c_prod_typ_ds 
,OriginalAppraisalAmount = COLL_AMT
,LTV = ORIG_LTV_RTO 
,JrLTV = 0 
,CombinedLTV = ORIG_LTV_RTO
,OriginalAmount = LN_AMT 
,Srbalance = 0 
,OriginalTerm = TERM
,AmorTerm = NULL 
,IOTerm = NULL 
,State = STATE
,Zip = 	zip
,FICO = ORIG_CRB_SCR
,UpdatedFICO = CUR_CRB_SCR
,Rate = CUR_INT_RT
,OriginationDate = LN_ORIG_DT
,PrepayPenaltyTerm = NULL
,MaxDrawnAmount = NULL 
,DrawdownTerm = NULL 
,DTI = TOT_DTI_RTO
,CurrentAmount = CUR_BAL_AMT
,ReportedLoanStatus = reportedloanstatus
,RealizedLoss = NULL
,FINAL_LGD_1  
,IsModified = NULL
,IsBroker = NULL
,IsHeloc = 0 
,IsHarris = 0 
,IsDefault = case when reportedloanstatus in ('defaulted','Write Off') then 1 else 0 end
,IsPrepaid = case when prepay = 'Y' then 1 else 0 end
,c_interest_only 
,ORIG_INT_RT 
,spec_pgm_id 
,modif_dt = null 
,cntrct_cd
from [ResearchDev].dbo.BMO_MI_MTG




insert into #temp0    
select 
LoanID = SK_ACCT_ID
,YYYY_MM
,Property = PROP_TYP_CD
,Occupancy = NULL
,Purpose = NULL 
,RateType = 'A'
,FixedRatePeriod = NULL
,LienPosition = C_1ST_LIEN 
,Documentation = PGM_TYP_CD 
,OriginalAppraisalAmount = c_COLL_AMT
,LTV = Srltv 
,JrLTV = Jrltv 
,CombinedLTV = ORIG_LTV_RTO
,OriginalAmount = INIT_DRAW_AMT 
,Srbalance = Srbalance 
,OriginalTerm = TERM
,AmorTerm = amorterm 
,IOTerm = IOterm 
,State = STATE
,Zip = 	zip
,FICO = ORIG_CRB_SCR
,UpdatedFICO = CUR_CRB_SCR
,Rate = CUR_INT_RT
,OriginationDate = LN_ORIG_DT
,PrepayPenaltyTerm = NULL
,MaxDrawnAmount = credit_limit 
,DrawdownTerm = CMTMNT_END_DT 
,DTI = TOT_DTI_RTO
,CurrentAmount = CUR_BAL_AMT
,ReportedLoanStatus = reportedloanstatus
,RealizedLoss = NULL
,FINAL_LGD_1  
,IsModified = NULL
,IsBroker = NULL
,IsHeloc = 1 
,IsHarris = 1 
,IsDefault = case when reportedloanstatus in ('defaulted','Write Off') then 1 else 0 end
,IsPrepaid = case when prepay = 'Y' then 1 else 0 end
,c_interest_only = null
,ORIG_INT_RT 
,spec_pgm_id = null
,modif_dt 
,cntrct_cd = NULL
from [ResearchDev].dbo.BMO_HARRIS_HELOC




insert into #temp0    
select 
LoanID = SK_ACCT_ID
,YYYY_MM
,Property = PROP_TYP_CD
,Occupancy = NULL 
,Purpose = NULL 
,RateType = 'A' 
,FixedRatePeriod = NULL
,LienPosition = C_1ST_LIEN 
,Documentation = PGM_TYP_CD 
,OriginalAppraisalAmount = c_COLL_AMT
,LTV = Srltv 
,JrLTV = Jrltv 
,CombinedLTV = ORIG_LTV_RTO
,OriginalAmount = INIT_DRAW_AMT 
,Srbalance = Srbalance 
,OriginalTerm = TERM
,AmorTerm = amorterm 
,IOTerm = IOterm 
,State = STATE
,Zip = 	zip
,FICO = ORIG_CRB_SCR
,UpdatedFICO = CUR_CRB_SCR
,Rate = CUR_INT_RT
,OriginationDate = LN_ORIG_DT
,PrepayPenaltyTerm = NULL
,MaxDrawnAmount = credit_limit 
,DrawdownTerm = CMTMNT_END_DT 
,DTI = TOT_DTI_RTO
,CurrentAmount = CUR_BAL_AMT
,ReportedLoanStatus = reportedloanstatus
,RealizedLoss = NULL
,FINAL_LGD_1  
,IsModified = NULL
,IsBroker = NULL
,IsHeloc = 1 
,IsHarris = 0 
,IsDefault = case when reportedloanstatus in ('defaulted','Write Off')  then 1 else 0 end
,IsPrepaid = case when prepay = 'Y' then 1 else 0 end
,c_interest_only = null
,ORIG_INT_RT 
,spec_pgm_id = null
,modif_dt 
,cntrct_cd = NULL
from [ResearchDev].dbo.BMO_MI_HELOC


If Object_id('[ResearchDev].dbo.BMO_RAW_ALL') is not null drop table [ResearchDev].dbo.BMO_RAW_ALL
select * 
into [ResearchDev].dbo.BMO_RAW_ALL
from #temp0

CREATE INDEX PIndex
ON [ResearchDev].dbo.BMO_RAW_ALL (LoanID, YYYY_MM)

-----------------------------------------------------Static Table-------------------------------------------------------------------
--If Object_id('tempdb.dbo.#RAW_ALL_Temp') is not null drop table #RAW_ALL_Temp
--select * 
--into #RAW_ALL_Temp
--from [ResearchDev].dbo.BMO_RAW_ALL

/*Truncate records after first DF/PRP/Closed/WO date/Expired observation*/
--Maturity Date
If Object_id('tempdb.dbo.#M_Dt') is not null drop table #M_Dt
select LoanID, max(YYYY_MM) as Mat_YYYY_MM
into #M_Dt
from [ResearchDev].dbo.BMO_RAW_ALL
group by LoanID

--First DF/PRP/Closed/WO date/Expired
If Object_id('tempdb.dbo.#cutoff') is not null drop table #cutoff
select a.LoanID, min(YYYY_MM) as cutoff_YYYY_MM 
into #cutoff
from [ResearchDev].dbo.BMO_RAW_ALL a 
left join #M_Dt b
on a.LoanID = b.LoanID 
where reportedloanstatus in ('Closed','Defaulted','Write Off')
or IsPrepaid = 1
or YYYY_MM = b.Mat_YYYY_MM
group by a.LoanID

--Cutoff data after first record of DF/PRP/Closed/WO date/Expired 
If Object_id('tempdb.dbo.#RAW_ALL_Temp') is not null drop table #RAW_ALL_Temp
select a.*
into #RAW_ALL_Temp
from [ResearchDev].dbo.BMO_RAW_ALL a 
left join #cutoff b
on a.LoanID = b.LoanID
where a.YYYY_MM <= b.cutoff_YYYY_MM
--or b.cutoff_YYYY_MM is null



--Generate Master Static Table
If Object_id('tempdb.dbo.#ST_Master') is not null drop table #ST_Master
select distinct LoanID 
into #ST_Master
from #RAW_ALL_Temp

--Variables List for first value
If Object_id('tempdb.dbo.#Var_ST_1st') is not null drop table #Var_ST_1st
CREATE TABLE #Var_ST_1st
(num int, Var_ST_1st varchar(255), Data_Type varchar(255))
insert into #Var_ST_1st
select 1,	'LienPosition',		'varchar(255)'	union all
select 2,	'c_interest_only',		'varchar(255)'


--Assign the first observation through loop
DECLARE @x  INT;
DECLARE @Var_ST_1st VARCHAR(255);
DECLARE @data_type VARCHAR(255);
Declare @sql nvarchar(4000);
Declare @sql2 nvarchar(4000);
SET @x = 1;
WHILE (@x) <=  (select count(*) from #Var_ST_1st)
BEGIN
          SET  @Var_ST_1st = (select Var_ST_1st from #Var_ST_1st where num = @x);
          SET  @data_type = (select data_type from #Var_ST_1st where num = @x);
          SET  @x = @x + 1;
          set @sql='ALTER TABLE #ST_Master ADD '+ @Var_ST_1st +' '+@data_type ;
          exec sp_executesql @sql;
          set @sql2 ='If Object_id(''tempdb.dbo.#t1'') is not null drop table #t1
				WITH summary as (select LoanID, min(YYYY_MM) as YYYY_MM_1st
				from #RAW_ALL_Temp
				where '+ @Var_ST_1st + ' <> '''' and '+ @Var_ST_1st + ' is not null
				group by LoanID)
				select a.LoanID,'+ @Var_ST_1st + ' as Var_ST_1st
				into #t1
				from #RAW_ALL_Temp a join summary b
				on a.LoanID = b.LoanID and a.YYYY_MM = b.YYYY_MM_1st

				update #ST_Master
				set  #ST_Master.'+ @Var_ST_1st + '= #t1.Var_ST_1st
				from  #ST_Master left join #t1
				on  #ST_Master.LoanID = #t1.LoanID
				'
				exec sp_executesql @sql2
END




--List of variables should not have 0
If Object_id('tempdb.dbo.#Var_ST_nonzero') is not null drop table #Var_ST_nonzero
CREATE TABLE #Var_ST_nonzero
(num int, Var_ST_nonzero varchar(255))
insert into #Var_ST_nonzero
select 1,	'OriginalAppraisalAmount' union all
select 2,	'LTV' union all
select 3,	'CombinedLTV' union all
select 4,	'OriginalAmount' union all
select 5,	'OriginalTerm' union all
select 6,	'AmorTerm' union all
select 7,	'FICO' union all
select 8,	'ORIG_INT_RT' union all
select 9,	'MaxDrawnAmount' union all
select 10,	'DTI'


DECLARE @x  INT;
DECLARE @Var_ST_nonzero VARCHAR(255);
Declare @sql nvarchar(4000);
SET @x = 1;
WHILE (@x) <=  (select count(*) from #Var_ST_nonzero)
BEGIN
          SET  @Var_ST_nonzero = (select Var_ST_nonzero from #Var_ST_nonzero where num = @x);
          SET  @x = @x + 1;
          set @sql='update #RAW_ALL_Temp
					set '+ @Var_ST_nonzero +'  = ''''
					where cast('+ @Var_ST_nonzero +' as float) = 0';
          exec sp_executesql @sql;          
END



--Additional two non-zero fields for 2nd Lien
update #RAW_ALL_Temp
set #RAW_ALL_Temp.JrLTV  = ''
from #RAW_ALL_Temp left join #ST_Master on #RAW_ALL_Temp.LoanID = #ST_Master.LoanID
where cast(JrLTV as float) = 0
and #ST_Master.LienPosition = 'N'

update #RAW_ALL_Temp
set #RAW_ALL_Temp.Srbalance  = ''
from #RAW_ALL_Temp left join #ST_Master on #RAW_ALL_Temp.LoanID = #ST_Master.LoanID
where cast(Srbalance as float) = 0
and #ST_Master.LienPosition = 'N'



--Variables List for most common value
If Object_id('tempdb.dbo.#Var_ST') is not null drop table #Var_ST
CREATE TABLE #Var_ST
(num int, Var_ST varchar(255), Data_Type varchar(255))
insert into #Var_ST
select 1,	'Property',		'varchar(255)'	union all
select 2,	'Occupancy',		'varchar(255)'	union all
select 3,	'Purpose',				'varchar(255)'	union all
select 4,	'RateType','varchar(255)'	union all
select 5,	'FixedRatePeriod',		'varchar(255)'	union all
select 6,	'Documentation',		'varchar(255)'	union all
select 7,	'OriginalAppraisalAmount',		'varchar(255)'	union all
select 8,	'LTV',			'varchar(255)'	union all
select 9,	'JrLTV',		'varchar(255)'	union all
select 10,	'CombinedLTV',			'varchar(255)'	union all
select 11,	'OriginalAmount',	'varchar(255)'	union all
select 12,	'Srbalance',	'varchar(255)'	union all
select 13,	'OriginalTerm',		'varchar(255)'	union all
select 14,	'AmorTerm',			'varchar(255)'	union all
select 15,	'IOTerm',		'varchar(255)'	union all
select 16,	'State',			'varchar(255)'	union all
select 17,	'Zip',			'varchar(255)' 	union all
select 18,	'FICO',		'varchar(255)'	union all
select 19,	'ORIG_INT_RT',		'varchar(255)' union all/*Rate*/
select 20,	'OriginationDate',		'varchar(255)' union all
select 21,	'PrepayPenaltyTerm',		'varchar(255)' union all
select 22,	'MaxDrawnAmount',		'varchar(255)' union all
select 23,	'DrawdownTerm',		'varchar(255)' union all
select 24,	'DTI',		'varchar(255)' union all
select 25,	'IsModified',		'varchar(255)' union all
select 26,	'IsBroker',		'varchar(255)' union all
select 27,	'IsHeloc',		'varchar(255)' union all
select 28,	'IsHarris',		'varchar(255)' union all
select 29,	'cntrct_cd',		'varchar(255)' 



--Aggregate by (choose one for most common or 1st observation)
--Assign the most common non-blank value through loop
DECLARE @x  INT;
DECLARE @Var_ST VARCHAR(255);
DECLARE @data_type VARCHAR(255);
Declare @sql nvarchar(4000);
Declare @sql2 nvarchar(4000);
SET @x = 1;
WHILE (@x) <=  (select count(*) from #Var_ST)
BEGIN
          SET  @Var_ST = (select Var_ST from #Var_ST where num = @x);
          SET  @data_type = (select data_type from #Var_ST where num = @x);
          SET  @x = @x + 1;
          set @sql='ALTER TABLE #ST_Master ADD '+ @Var_ST +' '+@data_type ;
          exec sp_executesql @sql;
          set @sql2 ='If Object_id(''tempdb.dbo.#temp1'') is not null drop table #temp1
				WITH summary as (select  LoanID,'+ @Var_ST + ' as Var_ST, count(*) as cnt
				,ROW_NUMBER() OVER(PARTITION BY LoanID
				ORDER BY count(*) DESC,'+ @Var_ST + ' DESC) AS rnk
				from #RAW_ALL_Temp
				where '+ @Var_ST + ' <> '''' and '+ @Var_ST + ' is not null
				and reportedloanstatus not in (''Defaulted'',''Write Off'',''closed'')
				and IsPrepaid <> 1
				group by LoanID,'+ @Var_ST + ')
				select LoanID,Var_ST
				into #temp1
				from summary 
				WHERE rnk = 1

				update #ST_Master
				set  #ST_Master.'+ @Var_ST + '= #temp1.Var_ST
				from  #ST_Master left join #temp1
				on  #ST_Master.LoanID = #temp1.LoanID
				'
				exec sp_executesql @sql2
END


--Create Clean Static Table 
If Object_id('[ResearchDev].dbo.BMO_ALL_ST') is not null drop table [ResearchDev].dbo.BMO_ALL_ST
select *
into [ResearchDev].dbo.BMO_ALL_ST
from #ST_Master

CREATE INDEX PIndex
ON [ResearchDev].dbo.BMO_ALL_ST (LoanID)

delete from [ResearchDev].dbo.BMO_ALL_ST where isHELOC is null

--select * into [ResearchDev].dbo.BMO_ALL_ST_bkp_06032015
--from [ResearchDev].dbo.BMO_ALL_ST

----------------------------------------------------Static Cleaning----------------------------------------------------
If Object_id('tempdb.dbo.#ST_Clean') is not null drop table #ST_Clean
select * 
into #ST_Clean
from [ResearchDev].dbo.BMO_ALL_ST

ALTER TABLE #ST_Clean ADD Ump_Region varchar(255)
ALTER TABLE #ST_Clean ADD HPI_Region varchar(255)


CREATE INDEX PIndex
ON #ST_Clean (LoanID)


/*Rate Type*/
--For both Harris and M&I HELOC portfolios, we will assume that the loans are ARM loans. 0.03% of the M&I HELOC loans are marked as FRM, which will be converted to ARM loans.
	--Have been taken care of when build the RAW table
--For fixed rate loans, set RateType = “F”, and for adjustable rate loans, set RateType = “A”.
update #ST_Clean
set RateType = 'F'
where RateType = '0'
and isHELOC = 0

update #ST_Clean
set RateType = 'A'
where RateType = '1'
and isHELOC = 0


/*Fixed Rate Period*/
--If ARM loans, then use cntrct_cd to map the FixedRatePeriod information. 
--If FRM loans, then set it as blank
update #ST_Clean
set #ST_Clean.FixedRatePeriod = b.FixedRatePeriod
from #ST_Clean a left join ContractCodeToFixedRatePeriod b
on a.cntrct_cd = cast(b.[Contract Code] as int)
where RateType = 'A'

update #ST_Clean
set FixedRatePeriod = ''
where RateType = 'F'

/*LienPosition*/
--If LienPosition/C_1ST_LIEN = “Y”, set them to be “1” (1st lien loans). If LienPosition/C_1ST_LIEN = “N”, set them to be “2” (2nd lien loans).
update #ST_Clean
set  LienPosition = case when LienPosition = 'Y' then 1 else 2 end 
from  #ST_Clean 
where isHELOC = 1






/*OriginalAppraisalAmount/LTV/JrLTV/CombinedLTV/MaxDrawnAmount/Srbalance*/

--mortgage portfolio:
--a)	If OriginalAppraisalAmount < 1,000 or = NULL/missing and OriginalAmount/LTV *100 is >= 1,000, then, let OriginalAppraisalAmount = OriginalAmount/LTV *100
update #ST_Clean
set OriginalAppraisalAmount = cast(OriginalAmount as float)/cast(LTV as float) *100
where  isheloc = 0
and (cast(OriginalAppraisalAmount as float) < 1000 or OriginalAppraisalAmount is NULL)
and (cast(OriginalAmount as float)/cast(LTV as float) *100 >= 1000)
and cast(LTV as float  ) > 0--(2116 row(s) affected)
--b)	If OriginalAmount < 1,000 or = NULL/missing and OriginalAppraisalAmount * LTV / 100 is >= 1,000, then, let OriginalAmount = OriginalAppraisalAmount * LTV / 100
update #ST_Clean
set OriginalAmount = cast(OriginalAppraisalAmount as float)*cast(LTV as float) /100
where isheloc = 0
and (cast(OriginalAmount as float) < 1000 or OriginalAmount is NULL)
and (cast(OriginalAppraisalAmount as float)*cast(LTV as float) /100 >= 1000)
and cast(LTV as float) > 0 --(0 row(s) affected)
--c)	If LTV <= 0 or = NULL/missing or > 130, and OriginalAmount/ OriginalAppraisalAmount * 100 is within the range ( > 0 and <= 130),OriginalAppraisalAmount >= 1,000 and OriginalAmount >= 1,000, then, let LTV = OriginalAmount/ OriginalAppraisalAmount * 100, then, let LTV = OriginalAmount/ OriginalAppraisalAmount * 100
update #ST_Clean
set LTV = cast(OriginalAmount as float)/cast(OriginalAppraisalAmount as float)* 100
--select LTV,(cast(OriginalAmount as float)/cast(OriginalAppraisalAmount as float)* 100),* from #ST_Clean
where isheloc = 0
and (cast(LTV as float) <= 0 or cast(LTV as float) > 130 or LTV is NULL)
and (cast(OriginalAmount as float)/cast(OriginalAppraisalAmount as float)* 100  between 0 and 130)
and cast(OriginalAppraisalAmount as float)>= 1000 and cast(OriginalAmount as float)>= 1000
and cast(OriginalAmount as float)> 0 -- (3447 row(s) affected)
	
--d)	Let CombinedLTV = LTV
update #ST_Clean
set CombinedLTV = LTV
--select * from #ST_Clean
where isheloc = 0


--HELOC portfolio:
--1st Lien Loans:
--a)	Let JrLTV = Srbalance = 0.
update #ST_Clean
set JrLTV = 0,
Srbalance =0
--select * from #ST_Clean
where isheloc = 1 and LienPosition = 1

--b)	If LTV <= 0 or = NULL/missing but CombinedLTV is within the range (> 0 and <= 130), then, let LTV = CombinedLTV. 
update #ST_Clean
set LTV = CombinedLTV
--select * from #ST_Clean
where  isheloc = 1 and LienPosition = 1
and (cast(LTV as float) <= 0 or cast(LTV as float) > 130 or LTV is NULL)
and (cast(CombinedLTV as float) > 0 and  cast(CombinedLTV as float) <= 130) --(602 row(s) affected)

--c)	If OriginalAppraisalAmount < 1,000 or = NULL/missing and MaxDrawnAmount/LTV *100 is >= 1,000, then, let OriginalAppraisalAmount = MaxDrawnAmount/LTV *100
update #ST_Clean
set OriginalAppraisalAmount = cast(MaxDrawnAmount as float)/cast(LTV as float) *100
--select * from #ST_Clean
where  isheloc = 1 and LienPosition = 1
and (cast(OriginalAppraisalAmount as float) < 1000 or OriginalAppraisalAmount is NULL)
and (cast(MaxDrawnAmount as float)/cast(LTV as float) *100 >= 1000)
and cast(LTV as float  ) > 0--(1 row(s) affected)

--d)	If MaxDrawnAmount < 1,000 or = NULL/missing and OriginalAppraisalAmount * LTV / 100 is >= 1,000, and OriginalAppraisalAmount >= 1,000 then, let MaxDrawnAmount = OriginalAppraisalAmount * LTV / 100
update #ST_Clean
set MaxDrawnAmount = cast(OriginalAppraisalAmount as float)*cast(LTV as float) /100
--select MaxDrawnAmount,cast(OriginalAppraisalAmount as float)*LTV /100,* from #ST_Clean
where isheloc = 1 and LienPosition = 1
and (cast(MaxDrawnAmount as float) < 1000 or MaxDrawnAmount is NULL)
and (cast(OriginalAppraisalAmount as float)*cast(LTV as float) /100 >= 1000)
and cast (OriginalAppraisalAmount as float)>= 1000
--e)	If LTV <= 0 or = NULL/missing or > 130, and MaxDrawnAmount/OriginalAppraisalAmount * 100 is within the range ( > 0 and <= 130) and MaxDrawnAmount >= 1,000, then, let LTV = MaxDrawnAmount/ OriginalAppraisalAmount * 100
update #ST_Clean
set LTV = cast(MaxDrawnAmount as float)/cast(OriginalAppraisalAmount as float)* 100
--select LTV,(cast(MaxDrawnAmount as float)/cast(OriginalAppraisalAmount as float)* 100),* from #ST_Clean
where isheloc = 1 and LienPosition = 1
and (cast(LTV as float) <= 0 or cast(LTV as float) > 130 or LTV is NULL)
and (cast(MaxDrawnAmount as float)/cast(OriginalAppraisalAmount as float)* 100  between 0 and 130)
and cast(OriginalAppraisalAmount as float)>= 1000
and cast(MaxDrawnAmount as float)>= 1000 -- (1838 row(s) affected)

--f)	Let CombinedLTV = LTV
update #ST_Clean
set CombinedLTV = LTV
--select * from #ST_Clean
where isheloc = 1 and LienPosition = 1


--2nd Lien Loans:
--a)	If LTV <= 0 or = NULL/missing and CombinedLTV – JrLTV is within the range (> 0 and <= 130), then, let LTV = CombinedLTV - JrLTV. 
update #ST_Clean
set LTV = cast(CombinedLTV as float) - cast(JrLTV as float)
--select LTV,cast(CombinedLTV as float)-cast(JrLTV as float),* from #ST_Clean
where isheloc = 1 and LienPosition = 2
and (cast(LTV as float) <= 0 or LTV is null) 
and (cast(CombinedLTV as float) - cast(JrLTV as float) > 0 and cast(CombinedLTV as float) - cast(JrLTV as float)<=130)--(95 row(s) affected)
--b)	If Srbalance <=0 or = NULL/missing and OriginalAppraisalAmount * LTV/100 is > 0,OriginalAppraisalAmount >= 1,000 then, let Srbalance = OriginalAppraisalAmount * LTV/100
update #ST_Clean
set Srbalance = cast(OriginalAppraisalAmount as float) * cast(LTV as float)/100
--select Srbalance,cast(OriginalAppraisalAmount as float) * cast(LTV as float)/100,* from #ST_Clean
where isheloc = 1 and LienPosition = 2
and (cast(Srbalance as float) <= 0 or Srbalance is NULL)
and cast(OriginalAppraisalAmount as float) * cast(LTV as float)/100 > 0 --(183 row(s) affected)
and cast(OriginalAppraisalAmount as float) >= 1000
--c)	If OriginalAppraisalAmount < 1,000 or = NULL/missing and Srbalance * 100 / LTV is >= 1,000, then, let OriginalAppraisalAmount = Srbalance * 100 / LTV
update #ST_Clean
set OriginalAppraisalAmount = cast(Srbalance as float) * 100 / cast(LTV as float)
--select OriginalAppraisalAmount ,cast(Srbalance as float) * 100 / cast(LTV as float),* from #ST_Clean
where isheloc = 1 and LienPosition = 2
and (cast(OriginalAppraisalAmount as float) < 1000 or OriginalAppraisalAmount is NULL)
and cast(Srbalance as float) * 100 / cast(LTV as float) >= 1000
and cast(LTV as float) > 0 --(1 row(s) affected)
--d)	If LTV <= 0 or = NULL/missing or > 130, and Srbalance / OriginalAppraisalAmount * 100 is within the range ( > 0 and <= 130) and OriginalAppraisalAmount  >= 1,000, then, let LTV = Srbalance / OriginalAppraisalAmount * 100
update #ST_Clean
set LTV = cast(Srbalance as float) / cast(OriginalAppraisalAmount as float) * 100
--select LTV ,cast(Srbalance as float) / cast(OriginalAppraisalAmount as float) * 100,* from #ST_Clean
where isheloc = 1 and LienPosition = 2
and (cast(LTV as float) <= 0 or LTV is NULL or cast(LTV as float) > 130)
and (cast(Srbalance as float) / cast(OriginalAppraisalAmount as float) * 100 > 0 and cast(Srbalance as float) / cast(OriginalAppraisalAmount as float) * 100 <= 130)
and cast(OriginalAppraisalAmount as float) >= 1000  -- (0 row(s) affected)
--e)	If JrLTV <= 0 or = NULL/missing and CombinedLTV – LTV is within the range (> 0 and <= 130), then, let JrLTV = CombinedLTV - LTV.
update #ST_Clean
set JrLTV = cast(CombinedLTV as float) - cast(LTV as float)
--select JrLTV,cast(CombinedLTV as float) - cast(LTV as float),* from #ST_Clean
where isheloc = 1 and LienPosition = 2
and (cast(JrLTV as float) <= 0 or JrLTV is NULL or cast(JrLTV as float) > 130)
and (cast(CombinedLTV as float) - cast(LTV as float) > 0 and cast(CombinedLTV as float) - cast(LTV as float) <= 130)
--f)	If MaxDrawnAmount <=0 or = NULL/missing and OriginalAppraisalAmount * JrLTV/100 is >= 1,000 and OriginalAppraisalAmount >= 1,000, then, let MaxDrawnAmount = OriginalAppraisalAmount * JrLTV/100
update #ST_Clean
set MaxDrawnAmount = cast(OriginalAppraisalAmount as float) * cast(JrLTV as float)/100
--select MaxDrawnAmount,cast(OriginalAppraisalAmount as float) * cast(JrLTV as float)/100,* from #ST_Clean
where isheloc = 1 and LienPosition = 2
and (cast(MaxDrawnAmount  as float) <= 0 or MaxDrawnAmount  is NULL )
and (cast(OriginalAppraisalAmount as float) * cast(JrLTV as float)/100 >= 1000)--(56 row(s) affected)
and cast(OriginalAppraisalAmount as float) >=1000
--g)	If OriginalAppraisalAmount < 1,000 or = NULL/missing and MaxDrawnAmount * 100 / LTV is >= 1,000, and MaxDrawnAmount >= 1,000 then, let OriginalAppraisalAmount = MaxDrawnAmount * 100 / LTV
update #ST_Clean
set OriginalAppraisalAmount = cast(MaxDrawnAmount as float) * 100 / cast(LTV as float)
--select OriginalAppraisalAmount ,cast(MaxDrawnAmount as float) * 100 / cast(LTV as float),* from #ST_Clean
where isheloc = 1 and LienPosition = 2
and (cast(OriginalAppraisalAmount  as float) < 1000 or OriginalAppraisalAmount is NULL )
and (cast(MaxDrawnAmount as float) * 100 / cast(LTV as float)) >= 1000
and cast(MaxDrawnAmount as float) >=1000
and cast(LTV as float) > 0 -- (0 row(s) affected)

--h)	If JrLTV <= 0 or = NULL/missing or > 130, and MaxDrawnAmount / OriginalAppraisalAmount * 100 is within the range ( > 0 and <= 130),and OriginalAppraisalAmount >= 1,000 then, let JrLTV = MaxDrawnAmount / OriginalAppraisalAmount * 100
update #ST_Clean
set JrLTV = cast(MaxDrawnAmount as float) / cast(OriginalAppraisalAmount as float) * 100
--select JrLTV,cast(MaxDrawnAmount as float) / cast(OriginalAppraisalAmount as float) * 100,* from #ST_Clean
--select * from #ST_Clean
where isheloc = 1 and LienPosition = 2 --213557
and (cast(JrLTV as float) <= 0 or JrLTV is NULL or cast(JrLTV as float) > 130)
and cast(MaxDrawnAmount as float) / cast(OriginalAppraisalAmount as float) * 100 > 0 and cast(MaxDrawnAmount as float) / cast(OriginalAppraisalAmount as float) * 100 <= 130
and cast(OriginalAppraisalAmount as float) >= 1000 -- 27128

--i)	Let CombinedLTV = LTV + JrLTV
update #ST_Clean
set CombinedLTV = cast(LTV as float) + cast(JrLTV as float)
where isheloc = 1 and LienPosition = 2 




/*OriginalAmount*/
delete 
--select *
from #ST_Clean
where isHELOC = 0
and (cast(OriginalAmount as float) <=0 or OriginalAmount is null)--(29 row(s) affected)

delete 
--select *
from #ST_Clean
where isHELOC = 1
and (cast(OriginalAmount as float) <0)--(15 row(s) affected)


/*Original Term*/
update #ST_Clean
set OriginalTerm = 360
where cast(OriginalTerm as int) <=60
and isHELOC = 0

/*IOTerm for MTG*/ 

--If the loans are IO loans, then, set IOTerm = FixedRatePeriod. Otherwise, set IOTerm = 0. 
update #ST_Clean
set IOTerm = FixedRatePeriod
where c_interest_only  = 1 -- (19257 row(s) affected)
and isHELOC = 0

update #ST_Clean
set #ST_Clean.IOTerm = 0
where c_interest_only  = 0 -- (249178 row(s) affected)
and isHELOC = 0


/*FICO at Orig*/
--Replace blank FICO scores at origination with the earliest non-blank valid updated FICO score, if available.
If Object_id('tempdb.dbo.#t1') is not null drop table #t1
WITH summary as (select LoanID, min(YYYY_MM) as YYYY_MM_1st
from [ResearchDev].dbo.BMO_RAW_ALL
where UpdatedFICO <> '' and UpdatedFICO is not null
and cast(UpdatedFICO as int) >= 300 and cast(UpdatedFICO as int) <= 850
group by LoanID)
select a.LoanID,UpdatedFICO
into #t1
from [ResearchDev].dbo.BMO_RAW_ALL a join summary b
on a.LoanID = b.LoanID and a.YYYY_MM = b.YYYY_MM_1st

update #ST_Clean
set  #ST_Clean.FICO= #t1.UpdatedFICO
--select * 
from  #ST_Clean left join #t1
on  #ST_Clean.LoanID = #t1.LoanID
where 
#ST_Clean.FICO is null or #ST_Clean.FICO = ''
or cast(#ST_Clean.FICO as int) < 300 or cast(#ST_Clean.FICO as int) > 850 




--Cap FICO at 850
update #ST_Clean
set FICO = 850
where cast(FICO as int) > 850 

--Floor FICO at 300
update #ST_Clean
set FICO = 300
where cast(FICO as int) < 300 



/*Rate (at origination)*/
--Interest rate will be capped at 25%.
update #ST_Clean
set ORIG_INT_RT = 25
where cast(ORIG_INT_RT as float) > 25


/*DrawdownTerm (for HELOCs)*/
--Calculate DrawdownTerm from CMTMNT_END_DT
update #ST_Clean
set DrawdownTerm = 
(year(CONVERT(datetime,CONVERT(datetime,DrawdownTerm,106),101)) - 
year(CONVERT(datetime,CONVERT(datetime,OriginationDate,106),101)))*12
+ month(CONVERT(datetime,CONVERT(datetime,DrawdownTerm,106),101)) -
month(CONVERT(datetime,CONVERT(datetime,OriginationDate,106),101))
where isHELOC = 1
 
--Replace blank values of DrawdownTerm by the clean value of OriginalTerm.
--If DrawdownTerm is greater than OriginalTerm, set DrawdownTerm to OriginalTerm.
update #ST_Clean
set DrawdownTerm = OriginalTerm
--select * from #ST_Clean
where isHELOC = 1 and 
(DrawdownTerm  is null
or cast(DrawdownTerm as int) > cast(OriginalTerm as int)
or DrawdownTerm in ('','0'))


/*IOTerm for HELOC*/
update #ST_Clean
set IOTerm = DrawdownTerm
where isHELOC = 1


/*DTI*/
--DTI will be capped at 100 for all portfolios.
update #ST_Clean
set DTI  = 100
where cast(DTI as float) > 100



/*HPI Region*/
--ALTER TABLE #ST_Clean ADD HPI_Region varchar(255)


update a
set a.HPI_Region = msa
from #ST_Clean a left join 
(select cast(ZIP as varchar(50)) as ZIP,msa from BMO_ZipMSA where msa in ('MCHI' /*Chicago*/,'MMIN' /*Minneapolis*/,'MPHO' /*Phoenix*/,'MMIL' /*Milwaukee*/,'MIND' /*Indianapolis*/)) b
on left(a.zip,5) COLLATE DATABASE_DEFAULT = b.ZIP 

update #ST_Clean
set HPI_Region = state
where state in ('IL','WI','MN','FL','AZ')
and HPI_Region is null 

update #ST_Clean
set HPI_Region = 'US'
where HPI_Region is null 

/*Ump Region*/
--ALTER TABLE #ST_Clean ADD Ump_Region varchar(255)

update #ST_Clean
set Ump_Region  = state
where state in ('IL','WI','MN','IN')
and Ump_Region is null

update #ST_Clean
set Ump_Region = 'US'
where Ump_Region is null


/*Create Clean Static Table*/
If Object_id('[ResearchDev].dbo.BMO_ALL_ST_Clean') is not null drop table [ResearchDev].dbo.BMO_ALL_ST_Clean
select 		
LoanID	
,LienPosition = cast(LienPosition as int)
,c_interest_only = cast(c_interest_only as int)
,Property	
,Occupancy	
,Purpose	
,RateType	
,FixedRatePeriod = case when FixedRatePeriod in ('','NULL') then NULL else cast(FixedRatePeriod as int) end
,Documentation	
,OriginalAppraisalAmount = cast(OriginalAppraisalAmount as float)
,LTV	= cast(LTV as float)
,JrLTV	= cast(JrLTV	as float)
,CombinedLTV	= cast(CombinedLTV	as float)
,OriginalAmount = cast(OriginalAmount as float)
,Srbalance	= cast(Srbalance	as float)
,OriginalTerm = cast(OriginalTerm as int)
,AmorTerm = cast(AmorTerm as int)
,IOTerm = case when IOTerm in ('','NULL') then NULL else cast(IOTerm as int) end
,State	
,Zip 
,FICO = cast(FICO as int)
,ORIG_INT_RT	= cast(ORIG_INT_RT as float)
,OriginationDate	
,PrepayPenaltyTerm	= cast(PrepayPenaltyTerm as int)
,MaxDrawnAmount	= cast(MaxDrawnAmount as float)
,DrawdownTerm = cast(DrawdownTerm as int)
,DTI = cast(DTI as float)
,IsModified	
,IsBroker	
,IsHeloc	
,IsHarris	
,cntrct_cd	
,HPI_Region	
,Ump_Region
into [ResearchDev].dbo.BMO_ALL_ST_Clean
from #ST_Clean


-----------------------------------------------------Dynamic Table-------------------------------------------------------------------
/*add IsHeloc and IsHarris to Dynamic tables*/


/*Truncate records after first DF/PRP/Closed/WO date/Expired observation*/
--Maturity Date
If Object_id('tempdb.dbo.#M_Dt') is not null drop table #M_Dt
select LoanID, max(YYYY_MM) as Mat_YYYY_MM
into #M_Dt
from [ResearchDev].dbo.BMO_RAW_ALL
group by LoanID

--First DF/PRP/Closed/WO date/Expired
If Object_id('tempdb.dbo.#cutoff') is not null drop table #cutoff
select a.LoanID, min(YYYY_MM) as cutoff_YYYY_MM 
into #cutoff
from [ResearchDev].dbo.BMO_RAW_ALL a 
left join #M_Dt b
on a.LoanID = b.LoanID 
where reportedloanstatus in ('Closed','Defaulted','Write Off')
or IsPrepaid = 1
or YYYY_MM = b.Mat_YYYY_MM
group by a.LoanID

--Cutoff data after first record of DF/PRP/Closed/WO date/Expired 
If Object_id('tempdb.dbo.#NET') is not null drop table #NET
select a.*
into #NET
from [ResearchDev].dbo.BMO_RAW_ALL a 
left join #cutoff b
on a.LoanID = b.LoanID
where a.YYYY_MM <= b.cutoff_YYYY_MM
--or b.cutoff_YYYY_MM is null




CREATE INDEX PIndex
ON #NET (LoanID, YYYY_MM)

/*Remove one-record loans*/
--Non-performing loans will be taken care of by this step
--All loanids with only one record, except yyyymm = 201501

If Object_id('tempdb.dbo.#del') is not null drop table #del
select LoanID
into #del
from #NET
where year(CONVERT(datetime,CONVERT(datetime,(OriginationDate),106),101)) * 100 + 
month(CONVERT(datetime,CONVERT(datetime,(OriginationDate),106),101)) < 201411
group by LoanID
having count(*) = 1

delete 
from #NET
where loanid in (select loanid  from #del)
or YYYY_MM < 200201






/*Spike on 201209*/
--If Object_id('tempdb.dbo.#ToDel') is not null drop table #ToDel
--select distinct LoanID 
--into #ToDel
--from #NET
--where Loanid in (select Loanid  from BMO_ALL_st where isheloc = 1)
--and LoanID in (select LoanID from #NET where YYYY_MM = 201209	and REPORTEDLOANSTATUS = 'defaulted')
--and LoanID in (select LoanID from #NET where YYYY_MM = 201208	and REPORTEDLOANSTATUS <> 'within 90 days past due')

--delete 
--from #NET
--where LoanID in (select distinct LoanID from #ToDel)



/*GAP*/
--YrM Table
If Object_id('tempdb.dbo.#Year') is not null drop table #Year
create table #Year  (Yr int)
insert into #Year values (2002)
insert into #Year values (2003)
insert into #Year values (2004)
insert into #Year values (2005)
insert into #Year values (2006)
insert into #Year values (2007)
insert into #Year values (2008)
insert into #Year values (2009)
insert into #Year values (2010)
insert into #Year values (2011)
insert into #Year values (2012)
insert into #Year values (2013)
insert into #Year values (2014)
insert into #Year values (2015)

If Object_id('tempdb.dbo.#Month') is not null drop table #Month
create table #Month (M  int)
insert into #Month values (1)
insert into #Month values (2)
insert into #Month values (3)
insert into #Month values (4)
insert into #Month values (5)
insert into #Month values (6)
insert into #Month values (7)
insert into #Month values (8)
insert into #Month values (9)
insert into #Month values (10)
insert into #Month values (11)
insert into #Month values (12)


--time Table, t starts from 200201
If Object_id('tempdb.dbo.#t1') is not null drop table #t1
select *, YrM=Yr*100+M ,t = (Yr-2002)*12+(M-0)
into #t1 
from #Year y
cross join #Month m
order by Yr*100+M


--2.b.table contains static information and MAXt, MINt
If Object_id('tempdb.dbo.#n0') is not null drop table #n0
select LoanID, max(t) as MAXt,min(t) as MINt,
year(CONVERT(datetime,CONVERT(datetime,max(OriginationDate),106),101)) * 100 + 
month(CONVERT(datetime,CONVERT(datetime,max(OriginationDate),106),101)) as OriginationDate
into #n0
from #NET left join #t1
on YYYY_MM = YrM
group by LoanID


If Object_id('tempdb.dbo.#n1') is not null drop table #n1
-- #n1 will have all the records for each loan from the first observation available to the last obs without any gap
select s.*,t.*,LoanAge = (floor(cast(YrM as int)/100.0)-floor(OriginationDate/100.0))*12+cast(YrM as int)%100-OriginationDate%100
into #n1   -- = select sum(Maxt-Mint+1) from #n1   
from #n0 s --   
cross join #t1 t
where t.t>=Mint and t.t<=Maxt
order by LoanID,YrM

If Object_id('tempdb.dbo.#w1') is not null drop table #w1
/*table #w1 is the new dynamic table that won’t have any gaps*/
select 
n.t
,n.LoanID
,n.YrM as YYYY_MM
,w.Rate
,w.CurrentAmount
,w.UpdatedFICO
,w.ReportedLoanStatus 
,w.IsPrepaid
,w.IsDefault
,Impute = case when w.YYYY_MM is NULL then 1 else 0 end
into #w1              
from #n1 n            
left join #NET w 
on n.LoanID = w.LoanID and n.YrM = w.YYYY_MM


/* #i1 has the closest t of future nonmissing observations to the imputed record*/
If Object_id('tempdb.dbo.#i1') is not null drop table #i1
select a.LoanID,a.t,a.YYYY_MM , MaxNonMissingt = max(b.t)
into #i1    -- 
from #w1 a
left join #w1 b
on a.LoanID = b.LoanID and a.t > b.t
where a.Impute = 1 and b.Impute = 0
group by a.LoanID,a.t,a.YYYY_MM


If Object_id('tempdb.dbo.#i2') is not null drop table #i2
select i.* , w.CurrentAmount,w.Rate,w.ReportedLoanStatus,w.isPrepaid
into #i2   -- 
from #i1 i
left outer join #w1 w
on i.LoanID = w.LoanID and i.MaxNonMissingt = w.t


update #w1
set  
#w1.CurrentAmount= #i2.CurrentAmount,
#w1.Rate= #i2.Rate,
#w1.ReportedLoanStatus= #i2.ReportedLoanStatus,
#w1.isPrepaid= #i2.isPrepaid
from  #w1 left join #i2
on  #w1.LoanID = #i2.LoanID
and #w1.t=#i2.t
where Impute = 1


/*OriginationDate*/
If Object_id('tempdb.dbo.#ToDel') is not null drop table #ToDel
select distinct a.loanid
into #ToDel
from #w1 a left join BMO_ALL_ST_CLEAN b on a.loanid = b.loanid
where year(OriginationDate)*100 + month(OriginationDate) > YYYY_MM

delete 
from #w1
where LoanID in (select distinct LoanID from #ToDel)

/*CurrentAmount*/
update #w1
set CurrentAmount = 0
where cast(CurrentAmount as float) < 0

/*Rate (dynamic)*/
update #w1
set Rate = 25
where cast(Rate as float) > 25

/*FICO (dynamic)*/
If Object_id('tempdb.dbo.#wf1') is not null drop table #wf1
select *, NULL as Impute_FICO 
into #wf1 
from #w1 

--1.	For every loan, if the first monthly value is missing, we will replace it with the FICO at origination.
--ALTER TABLE #wf1 ADD  Impute_FICO varchar(255) --for data cleaning only


update #wf1
set #wf1.UpdatedFICO = BMO_ALL_ST_Clean.FICO
from #wf1 
join #n0 
on #wf1.LoanID = #n0.LoanID and #wf1.t = #n0.mint
left join BMO_ALL_ST_Clean
on #wf1.LoanID = BMO_ALL_ST_Clean.LoanID
where cast(UpdatedFICO as int) in ('',0)  or UpdatedFICO is null

--2.	In any subsequent month, if the value is missing, we replace it by the FICO score in the previous month.
update #wf1
set Impute_FICO  = 1
where cast(UpdatedFICO as int) in ('',0) or UpdatedFICO is null

If Object_id('tempdb.dbo.#f1') is not null drop table #f1
select a.LoanID,a.t,a.YYYY_MM , MaxNonMissingt = max(b.t)
into #f1    -- 
from #wf1 a
left join #wf1 b
on a.LoanID = b.LoanID and a.t > b.t
where a.Impute_FICO = 1 and b.Impute_FICO is null
group by a.LoanID,a.t,a.YYYY_MM



If Object_id('tempdb.dbo.#f2') is not null drop table #f2
select f.* , w.UpdatedFICO
into #f2   -- 
from #f1 f
left join #wf1 w
on f.LoanID = w.LoanID and f.MaxNonMissingt = w.t

update #wf1
set  
#wf1.UpdatedFICO= #f2.UpdatedFICO
from  #wf1 left join #f2
on  #wf1.LoanID = #f2.LoanID
and #wf1.t=#f2.t
where Impute_FICO = 1


--Clear the delinquency status
--a.ReportedLoanStatus before Default must be 60DPD
update a 
set a.reportedloanstatus  = 'within 90 days past due'
--select * 
from #wf1 a left join #wf1 b
on a.LoanID = b.LoanID and a.t = b.t -1  
where b.reportedloanstatus in ('Defaulted','Write Off')

--b.if ReportedLoanStatus before 60DPD is current, replace it by 30DPD
update a 
set a.reportedloanstatus  = 'within 60 days past due'
--select * 
from #wf1 a left join #wf1 b
on a.LoanID = b.LoanID and a.t = b.t -1  
where b.reportedloanstatus in ('within 90 days past due')
and a.reportedloanstatus not in ('within 90 days past due','within 60 days past due') 



----Remove Inactive Accounts (entire history)
If Object_id('tempdb.dbo.#To_Del_Inactive') is not null drop table #To_Del_Inactive
select distinct loanid into #To_Del_Inactive
from #wf1 where reportedloanstatus ='inactive'

delete
--select * 
from #wf1
where LoanID in (select * from #To_Del_Inactive)

--Remove Loans not exist in static table
delete
--select * 
from #wf1
where LoanID not in (select distinct loanid from BMO_ALL_ST_CLEAN)


--Create Dynamic Table 
If Object_id('[ResearchDev].dbo.BMO_ALL_DY_Clean') is not null drop table [ResearchDev].dbo.BMO_ALL_DY_Clean
select 
a.t	
,a.LoanID	
,a.YYYY_MM	
,Rate = case when a.Rate in ('','NULL') then NULL else cast(a.Rate as float) end
,CurrentAmount = case when a.CurrentAmount in ('','NULL') then NULL else cast(a.CurrentAmount as float) end
,UpdatedFICO = cast(a.UpdatedFICO as int)
,a.ReportedLoanStatus	
,a.IsPrepaid	
,a.IsDefault	
,a.Impute	
,a.Impute_FICO
,b.isHELOC,b.isHarris
into [ResearchDev].dbo.BMO_ALL_DY_Clean
from #wf1 a left join BMO_ALL_ST_Clean B
on a.loanid = b.loanid
and b.isHELOC is not null

CREATE INDEX PIndex
ON [ResearchDev].dbo.BMO_ALL_DY_Clean (LoanID, YYYY_MM)

--Remove Loans not exist in static table
delete
--select * 
from BMO_ALL_ST_CLEAN
where LoanID not in (select distinct loanid from BMO_ALL_DY_Clean)


----------------------------LGD----------------------------
If Object_id('tempdb.dbo.#L1') is not null drop table #L1
select loanid, min(YYYY_MM) as min_def_YM
into #L1
from [ResearchDev].dbo.BMO_RAW_ALL
where reportedloanstatus in ('Defaulted','Write Off')
group by loanid

If Object_id('tempdb.dbo.#L2') is not null drop table #L2
select loanid, max(YYYY_MM) as max_pos_LGD_YM
into #L2
from [ResearchDev].dbo.BMO_RAW_ALL
where cast(FINAL_LGD_1 as float) > 0
group by loanid

If Object_id('tempdb.dbo.#L3') is not null drop table #L3
select #L1.LoanID, min_def_YM as Def_YM, max_pos_LGD_YM
into #L3
from #L1 
left join #L2 on #L1.LoanID = #L2.LoanID

If Object_id('tempdb.dbo.#L4') is not null drop table #L4
select l.*,def.FINAL_LGD_1 as def_LGD,def.CurrentAmount as def_Amount,pos_LGD.FINAL_LGD_1 as pos_LGD
into #L4
from #L3 l left join [ResearchDev].dbo.BMO_RAW_ALL def on l.LoanID = def.LoanID and  l.Def_YM = def.YYYY_MM
left join [ResearchDev].dbo.BMO_RAW_ALL pos_LGD on l.LoanID = pos_LGD.LoanID and  l.max_pos_LGD_YM = pos_LGD.YYYY_MM

update #L4
set def_LGD = null
where def_LGD = ''

If Object_id('tempdb.dbo.#L5') is not null drop table #L5
select *
,LGD = cast(def_LGD as float) 
,Discount_t = (floor(cast(max_pos_LGD_YM as int)/100)-floor(def_YM/100))*12
+cast(max_pos_LGD_YM as int)%100-cast(def_YM as int)%100
,Discount_factor = 1/POWER(1+cast(.1 as float)/12, (floor(cast(max_pos_LGD_YM as int)/100)-floor(def_YM/100))*12
+cast(max_pos_LGD_YM as int)%100-cast(def_YM as int)%100)    
into #L5 from #L4

--if def_YM <> min_pos_LGD_YM, then LGD = 1-discounted RR
update #L5
set LGD = 1- (1 - cast(pos_LGD as float))*Discount_factor -- Only recovery rate is discounted
--select * 
from #L5
where max_pos_LGD_YM is not null and def_YM <> max_pos_LGD_YM --1623



--Remove Loans not exist in static table
delete
--select * 
from #L5
where 1=1
and LoanID not in (select distinct loanid from BMO_ALL_ST_CLEAN)

delete
--select * 
from #L5
where LoanID COLLATE DATABASE_DEFAULT in (select loanid from BMO_ShortList_All)



If Object_id('[ResearchDev].dbo.BMO_ALL_LGD') is not null drop table [ResearchDev].dbo.BMO_ALL_LGD
select LoanID = cast(LoanID as int)
,Def_YM = cast(Def_YM as int)
,max_pos_LGD_YM  =cast(max_pos_LGD_YM as int)
,def_LGD = cast(def_LGD as float)
,def_Amount = case when def_Amount = '' then null else cast(def_Amount as float) end
,pos_LGD = cast(pos_LGD as float)
,LGD = cast(LGD as float)
,Discount_t = cast(Discount_t as float)
,Discount_factor = cast(Discount_factor as float)
into [ResearchDev].dbo.BMO_ALL_LGD 
from #L5 




-----------------------------------------------------Macro------------------------------------------------------------
--YrM Table
If Object_id('tempdb.dbo.#Year') is not null drop table #Year
create table #Year  (Yr int)
insert into #Year values (1966)	insert into #Year values (1967)	insert into #Year values (1968)	insert into #Year values (1969)	insert into #Year values (1970)	insert into #Year values (1971)	insert into #Year values (1972)	insert into #Year values (1973)	insert into #Year values (1974)	insert into #Year values (1975)	insert into #Year values (1976)	insert into #Year values (1977)	insert into #Year values (1978)	insert into #Year values (1979)	insert into #Year values (1980)	insert into #Year values (1981)	insert into #Year values (1982)	insert into #Year values (1983)	insert into #Year values (1984)	insert into #Year values (1985)	insert into #Year values (1986)	insert into #Year values (1987)	insert into #Year values (1988)	insert into #Year values (1989)	insert into #Year values (1990)	insert into #Year values (1991)	insert into #Year values (1992)	insert into #Year values (1993)	insert into #Year values (1994)	insert into #Year values (1995)	insert into #Year values (1996)	insert into #Year values (1997)	insert into #Year values (1998)	insert into #Year values (1999)	insert into #Year values (2000)	insert into #Year values (2001)	insert into #Year values (2002)	insert into #Year values (2003)	insert into #Year values (2004)	insert into #Year values (2005)	insert into #Year values (2006)	insert into #Year values (2007)	insert into #Year values (2008)	insert into #Year values (2009)	insert into #Year values (2010)	insert into #Year values (2011)	insert into #Year values (2012)	insert into #Year values (2013)	insert into #Year values (2014) insert into #Year values (2015) insert into #Year values (2016) insert into #Year values (2017) insert into #Year values (2018)

If Object_id('tempdb.dbo.#Month') is not null drop table #Month
create table #Month (M  int)
insert into #Month values (1) insert into #Month values (2) insert into #Month values (3) insert into #Month values (4) insert into #Month values (5) insert into #Month values (6) insert into #Month values (7) insert into #Month values (8) insert into #Month values (9) insert into #Month values (10) insert into #Month values (11) insert into #Month values (12)

--time Table, t starts from 200201
If Object_id('tempdb.dbo.#t1') is not null drop table #t1
select *, YrM=Yr*100+M ,t = (Yr-2002)*12+(M-0)
into #t1 
from #Year y
cross join #Month m
order by Yr*100+M


--Linear Interpolation
If Object_id('tempdb.dbo.#m1') is not null drop table #m1
select 
n.YrM
,Impute = case when m.YYYY_MM is NULL then 1 else 0 end,
n.t,m.*
into #m1              
from #t1 n            
left join dbo.BMO_Macro m 
on n.YrM = m.YYYY_MM
order by t


If Object_id('tempdb.dbo.#m2') is not null drop table #m2
select cur.YrM,cur.t
,[GDP] = case when cur.[GDP] is null then pre.[GDP] + (nxt.[GDP] - pre.[GDP])*(cur.t - pre.t)/(nxt.t - pre.t) else cur.[GDP] end
,[CPI] = case when cur.[CPI] is null then pre.[CPI] + (nxt.[CPI] - pre.[CPI])*(cur.t - pre.t)/(nxt.t - pre.t) else cur.[CPI] end
,[Fed_Funds_Target_Rate] = case when cur.[Fed_Funds_Target_Rate] is null then pre.[Fed_Funds_Target_Rate] + (nxt.[Fed_Funds_Target_Rate] - pre.[Fed_Funds_Target_Rate])*(cur.t - pre.t)/(nxt.t - pre.t) else cur.[Fed_Funds_Target_Rate] end
,[TreasuryBill_1m] = case when cur.[TreasuryBill_1m] is null then pre.[TreasuryBill_1m] + (nxt.[TreasuryBill_1m] - pre.[TreasuryBill_1m])*(cur.t - pre.t)/(nxt.t - pre.t) else cur.[TreasuryBill_1m] end
,[TreasuryBill_3m] = case when cur.[TreasuryBill_3m] is null then pre.[TreasuryBill_3m] + (nxt.[TreasuryBill_3m] - pre.[TreasuryBill_3m])*(cur.t - pre.t)/(nxt.t - pre.t) else cur.[TreasuryBill_3m] end
,[Libor_1m] = case when cur.[Libor_1m] is null then pre.[Libor_1m] + (nxt.[Libor_1m] - pre.[Libor_1m])*(cur.t - pre.t)/(nxt.t - pre.t) else cur.[Libor_1m] end
,[Libor_3m] = case when cur.[Libor_3m] is null then pre.[Libor_3m] + (nxt.[Libor_3m] - pre.[Libor_3m])*(cur.t - pre.t)/(nxt.t - pre.t) else cur.[Libor_3m] end
,[TedSpread_1m] = case when cur.[TedSpread_1m] is null then pre.[TedSpread_1m] + (nxt.[TedSpread_1m] - pre.[TedSpread_1m])*(cur.t - pre.t)/(nxt.t - pre.t) else cur.[TedSpread_1m] end
,[TedSpread] = case when cur.[TedSpread] is null then pre.[TedSpread] + (nxt.[TedSpread] - pre.[TedSpread])*(cur.t - pre.t)/(nxt.t - pre.t) else cur.[TedSpread] end
,[Libor_6M] = case when cur.[Libor_6M] is null then pre.[Libor_6M] + (nxt.[Libor_6M] - pre.[Libor_6M])*(cur.t - pre.t)/(nxt.t - pre.t) else cur.[Libor_6M] end
,[Libor_12M] = case when cur.[Libor_12M] is null then pre.[Libor_12M] + (nxt.[Libor_12M] - pre.[Libor_12M])*(cur.t - pre.t)/(nxt.t - pre.t) else cur.[Libor_12M] end
,[Treasury_2Y] = case when cur.[Treasury_2Y] is null then pre.[Treasury_2Y] + (nxt.[Treasury_2Y] - pre.[Treasury_2Y])*(cur.t - pre.t)/(nxt.t - pre.t) else cur.[Treasury_2Y] end
,[Treasury_5Y] = case when cur.[Treasury_5Y] is null then pre.[Treasury_5Y] + (nxt.[Treasury_5Y] - pre.[Treasury_5Y])*(cur.t - pre.t)/(nxt.t - pre.t) else cur.[Treasury_5Y] end
,[Treasury_7Y] = case when cur.[Treasury_7Y] is null then pre.[Treasury_7Y] + (nxt.[Treasury_7Y] - pre.[Treasury_7Y])*(cur.t - pre.t)/(nxt.t - pre.t) else cur.[Treasury_7Y] end
,[Treasury_10Y] = case when cur.[Treasury_10Y] is null then pre.[Treasury_10Y] + (nxt.[Treasury_10Y] - pre.[Treasury_10Y])*(cur.t - pre.t)/(nxt.t - pre.t) else cur.[Treasury_10Y] end
,[Treasury_30Y] = case when cur.[Treasury_30Y] is null then pre.[Treasury_30Y] + (nxt.[Treasury_30Y] - pre.[Treasury_30Y])*(cur.t - pre.t)/(nxt.t - pre.t) else cur.[Treasury_30Y] end
,[SwapRate_2Y] = case when cur.[SwapRate_2Y] is null then pre.[SwapRate_2Y] + (nxt.[SwapRate_2Y] - pre.[SwapRate_2Y])*(cur.t - pre.t)/(nxt.t - pre.t) else cur.[SwapRate_2Y] end
,[SwapRate_5Y] = case when cur.[SwapRate_5Y] is null then pre.[SwapRate_5Y] + (nxt.[SwapRate_5Y] - pre.[SwapRate_5Y])*(cur.t - pre.t)/(nxt.t - pre.t) else cur.[SwapRate_5Y] end
,[SwapRate_7Y] = case when cur.[SwapRate_7Y] is null then pre.[SwapRate_7Y] + (nxt.[SwapRate_7Y] - pre.[SwapRate_7Y])*(cur.t - pre.t)/(nxt.t - pre.t) else cur.[SwapRate_7Y] end
,[SwapRate_10Y] = case when cur.[SwapRate_10Y] is null then pre.[SwapRate_10Y] + (nxt.[SwapRate_10Y] - pre.[SwapRate_10Y])*(cur.t - pre.t)/(nxt.t - pre.t) else cur.[SwapRate_10Y] end
,[SwapRate_30Y] = case when cur.[SwapRate_30Y] is null then pre.[SwapRate_30Y] + (nxt.[SwapRate_30Y] - pre.[SwapRate_30Y])*(cur.t - pre.t)/(nxt.t - pre.t) else cur.[SwapRate_30Y] end
,[ARM_1Y] = case when cur.[ARM_1Y] is null then pre.[ARM_1Y] + (nxt.[ARM_1Y] - pre.[ARM_1Y])*(cur.t - pre.t)/(nxt.t - pre.t) else cur.[ARM_1Y] end
,[ARM_5Y] = case when cur.[ARM_5Y] is null then pre.[ARM_5Y] + (nxt.[ARM_5Y] - pre.[ARM_5Y])*(cur.t - pre.t)/(nxt.t - pre.t) else cur.[ARM_5Y] end
,[MortgageRate_15Y] = case when cur.[MortgageRate_15Y] is null then pre.[MortgageRate_15Y] + (nxt.[MortgageRate_15Y] - pre.[MortgageRate_15Y])*(cur.t - pre.t)/(nxt.t - pre.t) else cur.[MortgageRate_15Y] end
,[MortgageRate_30Y] = case when cur.[MortgageRate_30Y] is null then pre.[MortgageRate_30Y] + (nxt.[MortgageRate_30Y] - pre.[MortgageRate_30Y])*(cur.t - pre.t)/(nxt.t - pre.t) else cur.[MortgageRate_30Y] end
,[Real_Fed_Funds_Target_Rate] = case when cur.[Real_Fed_Funds_Target_Rate] is null then pre.[Real_Fed_Funds_Target_Rate] + (nxt.[Real_Fed_Funds_Target_Rate] - pre.[Real_Fed_Funds_Target_Rate])*(cur.t - pre.t)/(nxt.t - pre.t) else cur.[Real_Fed_Funds_Target_Rate] end
,[Real_Treasury_10Y] = case when cur.[Real_Treasury_10Y] is null then pre.[Real_Treasury_10Y] + (nxt.[Real_Treasury_10Y] - pre.[Real_Treasury_10Y])*(cur.t - pre.t)/(nxt.t - pre.t) else cur.[Real_Treasury_10Y] end
,[Prime_Rate] = case when cur.[Prime_Rate] is null then pre.[Prime_Rate] + (nxt.[Prime_Rate] - pre.[Prime_Rate])*(cur.t - pre.t)/(nxt.t - pre.t) else cur.[Prime_Rate] end
,[Ump_US] = case when cur.[Ump_US] is null then pre.[Ump_US] + (nxt.[Ump_US] - pre.[Ump_US])*(cur.t - pre.t)/(nxt.t - pre.t) else cur.[Ump_US] end
,[Ump_IL] = case when cur.[Ump_IL] is null then pre.[Ump_IL] + (nxt.[Ump_IL] - pre.[Ump_IL])*(cur.t - pre.t)/(nxt.t - pre.t) else cur.[Ump_IL] end
,[Ump_MN] = case when cur.[Ump_MN] is null then pre.[Ump_MN] + (nxt.[Ump_MN] - pre.[Ump_MN])*(cur.t - pre.t)/(nxt.t - pre.t) else cur.[Ump_MN] end
,[Ump_WI] = case when cur.[Ump_WI] is null then pre.[Ump_WI] + (nxt.[Ump_WI] - pre.[Ump_WI])*(cur.t - pre.t)/(nxt.t - pre.t) else cur.[Ump_WI] end
,[Ump_IN] = case when cur.[Ump_IN] is null then pre.[Ump_IN] + (nxt.[Ump_IN] - pre.[Ump_IN])*(cur.t - pre.t)/(nxt.t - pre.t) else cur.[Ump_IN] end
,[HPI_US] = case when cur.[HPI_US] is null then pre.[HPI_US] + (nxt.[HPI_US] - pre.[HPI_US])*(cur.t - pre.t)/(nxt.t - pre.t) else cur.[HPI_US] end
,[HPI_IL] = case when cur.[HPI_IL] is null then pre.[HPI_IL] + (nxt.[HPI_IL] - pre.[HPI_IL])*(cur.t - pre.t)/(nxt.t - pre.t) else cur.[HPI_IL] end
,[HPI_WI] = case when cur.[HPI_WI] is null then pre.[HPI_WI] + (nxt.[HPI_WI] - pre.[HPI_WI])*(cur.t - pre.t)/(nxt.t - pre.t) else cur.[HPI_WI] end
,[HPI_MN] = case when cur.[HPI_MN] is null then pre.[HPI_MN] + (nxt.[HPI_MN] - pre.[HPI_MN])*(cur.t - pre.t)/(nxt.t - pre.t) else cur.[HPI_MN] end
,[HPI_FL] = case when cur.[HPI_FL] is null then pre.[HPI_FL] + (nxt.[HPI_FL] - pre.[HPI_FL])*(cur.t - pre.t)/(nxt.t - pre.t) else cur.[HPI_FL] end
,[HPI_AZ] = case when cur.[HPI_AZ] is null then pre.[HPI_AZ] + (nxt.[HPI_AZ] - pre.[HPI_AZ])*(cur.t - pre.t)/(nxt.t - pre.t) else cur.[HPI_AZ] end
,[HPI_Chicago] = case when cur.[HPI_Chicago] is null then pre.[HPI_Chicago] + (nxt.[HPI_Chicago] - pre.[HPI_Chicago])*(cur.t - pre.t)/(nxt.t - pre.t) else cur.[HPI_Chicago] end
,[HPI_Minneapolis] = case when cur.[HPI_Minneapolis] is null then pre.[HPI_Minneapolis] + (nxt.[HPI_Minneapolis] - pre.[HPI_Minneapolis])*(cur.t - pre.t)/(nxt.t - pre.t) else cur.[HPI_Minneapolis] end
,[HPI_Phoenix] = case when cur.[HPI_Phoenix] is null then pre.[HPI_Phoenix] + (nxt.[HPI_Phoenix] - pre.[HPI_Phoenix])*(cur.t - pre.t)/(nxt.t - pre.t) else cur.[HPI_Phoenix] end
,[HPI_Milwaukee] = case when cur.[HPI_Milwaukee] is null then pre.[HPI_Milwaukee] + (nxt.[HPI_Milwaukee] - pre.[HPI_Milwaukee])*(cur.t - pre.t)/(nxt.t - pre.t) else cur.[HPI_Milwaukee] end
,[HPI_Indianapolis] = case when cur.[HPI_Indianapolis] is null then pre.[HPI_Indianapolis] + (nxt.[HPI_Indianapolis] - pre.[HPI_Indianapolis])*(cur.t - pre.t)/(nxt.t - pre.t) else cur.[HPI_Indianapolis] end
into #m2  
from 
#m1  cur
left join #m1  pre
on cur.impute=1 and pre.impute=0 and cur.t > pre.t and cur.t <= pre.t + 2
left join #m1  nxt
on cur.impute=1 and nxt.impute=0 and cur.t < nxt.t and cur.t >= nxt.t - 2
--where cur.YrM between 200001 and 201412
order by cur.t

--Growth Rate
If Object_id('tempdb.dbo.#m3') is not null drop table #m3
select cur.* 
,[dUmp_US] = cur.Ump_US - pre1.Ump_US
,dUmpyoy_US = cur.Ump_US - pre12.Ump_US
,[dUmp_IL] = cur.Ump_IL - pre1.Ump_IL
,dUmpyoy_IL = cur.Ump_IL - pre12.Ump_IL
,[dUmp_MN] = cur.Ump_MN - pre1.Ump_MN
,dUmpyoy_MN = cur.Ump_MN - pre12.Ump_MN
,[dUmp_WI] = cur.Ump_WI - pre1.Ump_WI
,dUmpyoy_WI = cur.Ump_WI - pre12.Ump_WI
,[dUmp_IN] = cur.Ump_IN - pre1.Ump_IN
,dUmpyoy_IN = cur.Ump_IN - pre12.Ump_IN
,gCPI = log(cur.CPI) - log(pre1.CPI)
,gCPIyoy = log(cur.CPI) - log(pre12.CPI)
,gHPI_US = log(cur.HPI_US) - log(pre1.HPI_US)	
,gHPIyoy_US = log(cur.HPI_US) - log(pre12.HPI_US)	
,gHPI_IL	 = log(cur.HPI_IL) - log(pre1.HPI_IL)	
,gHPIyoy_IL	= log(cur.HPI_IL) - log(pre12.HPI_IL)	
,gHPI_WI	 = log(cur.HPI_WI) - log(pre1.HPI_WI)	
,gHPIyoy_WI	= log(cur.HPI_WI) - log(pre12.HPI_WI)	
,gHPI_MN	 = log(cur.HPI_MN) - log(pre1.HPI_MN)	
,gHPIyoy_MN	= log(cur.HPI_MN) - log(pre12.HPI_MN)	
,gHPI_FL	 = log(cur.HPI_FL) - log(pre1.HPI_FL)	
,gHPIyoy_FL	= log(cur.HPI_FL) - log(pre12.HPI_FL)	
,gHPI_AZ	 = log(cur.HPI_AZ) - log(pre1.HPI_AZ)	
,gHPIyoy_AZ	= log(cur.HPI_AZ) - log(pre12.HPI_AZ)	
,gHPI_Chicago	 = log(cur.HPI_Chicago) - log(pre1.HPI_Chicago)	
,gHPIyoy_Chicago	= log(cur.HPI_Chicago) - log(pre12.HPI_Chicago)	
,gHPI_Minneapolis	 = log(cur.HPI_Minneapolis) - log(pre1.HPI_Minneapolis)	
,gHPIyoy_Minneapolis	= log(cur.HPI_Minneapolis) - log(pre12.HPI_Minneapolis)	
,gHPI_Phoenix = log(cur.HPI_Phoenix) - log(pre1.HPI_Phoenix)	
,gHPIyoy_Phoenix= log(cur.HPI_Phoenix) - log(pre12.HPI_Phoenix)	
,gHPI_Milwaukee = log(cur.HPI_Milwaukee) - log(pre1.HPI_Milwaukee)	
,gHPIyoy_Milwaukee= log(cur.HPI_Milwaukee) - log(pre12.HPI_Milwaukee)	
,gHPI_Indianapolis = log(cur.HPI_Indianapolis) - log(pre1.HPI_Indianapolis)	
,gHPIyoy_Indianapolis= log(cur.HPI_Indianapolis) - log(pre12.HPI_Indianapolis)	
into #m3
from #m2 cur 
join #m2 pre1 on cur.t = pre1.t + 1
join #m2 pre12 on cur.t = pre12.t + 12


If Object_id('[ResearchDev].dbo.BMO_MACRO_M') is not null drop table [ResearchDev].dbo.BMO_MACRO_M
select * into [ResearchDev].dbo.BMO_MACRO_M from #m3



--Mapping 
--if zip in ('Chicago','Minneapolis','Phoenix','Milwaukee','Indianapolis') then use MSA level HPI   	

--else if state in ('IL','WI','MN','FL','AZ') then use state level HPI
--otherwise, use national HPI

If Object_id('[ResearchDev].dbo.BMO_HPI') is not null drop table [ResearchDev].dbo.BMO_HPI
select b.t,a.* 
into [ResearchDev].dbo.BMO_HPI
from (select YrM,HPI_US as HPI,gHPI_US as gHPI,gHPIyoy_US as gHPIyoy, 'US' as Region from BMO_Macro_m union
select YrM,HPI_IL as HPI,gHPI_IL as gHPI,gHPIyoy_IL as gHPIyoy, 'IL' as Region from BMO_Macro_m union
select YrM,HPI_WI as HPI,gHPI_WI as gHPI,gHPIyoy_WI as gHPIyoy, 'WI' as Region from BMO_Macro_m union
select YrM,HPI_MN as HPI,gHPI_MN as gHPI, gHPIyoy_MN as gHPIyoy, 'MN' as Region from BMO_Macro_m union
select YrM,HPI_FL as HPI,gHPI_FL as gHPI,gHPIyoy_FL as gHPIyoy, 'FL' as Region from BMO_Macro_m union
select YrM,HPI_AZ as HPI,gHPI_AZ as gHPI,gHPIyoy_AZ as gHPIyoy, 'AZ' as Region from BMO_Macro_m union
select YrM,HPI_Chicago as HPI,gHPI_Chicago as gHPI,gHPIyoy_Chicago as gHPIyoy, 'MCHI' as Region from BMO_Macro_m union
select YrM,HPI_Minneapolis as HPI,gHPI_Minneapolis as gHPI,gHPIyoy_Minneapolis as gHPIyoy, 'MMIN' as Region from BMO_Macro_m union
select YrM,HPI_Phoenix as HPI,gHPI_Phoenix as gHPI,gHPIyoy_Phoenix as gHPIyoy, 'MPHO' as Region from BMO_Macro_m union
select YrM,HPI_Milwaukee as HPI,gHPI_Milwaukee as gHPI,gHPIyoy_Milwaukee as gHPIyoy, 'MMIL' as Region from BMO_Macro_m union
select YrM,HPI_Indianapolis as HPI,gHPI_Indianapolis as gHPI,gHPIyoy_Indianapolis as gHPIyoy, 'MIND' as Region from BMO_Macro_m 
)a
left join BMO_YrM b on a.YrM = b.YrM
order by Region,a.YrM


If Object_id('[ResearchDev].dbo.BMO_Ump') is not null drop table [ResearchDev].dbo.BMO_Ump
select b.t,a.* 
into [ResearchDev].dbo.BMO_Ump
from (select YrM,Ump_US as Ump,dUmp_US as [dUmp],dUmpyoy_US as dUmpyoy, 'US' as Region from BMO_Macro_m union
select YrM,Ump_IL as Ump,dUmp_IL as [dUmp],dUmpyoy_IL as dUmpyoy, 'IL' as Region from BMO_Macro_m union
select YrM,Ump_MN as Ump,dUmp_MN as [dUmp],dUmpyoy_MN as dUmpyoy, 'MN' as Region from BMO_Macro_m union
select YrM,Ump_WI as Ump,dUmp_WI as [dUmp],dUmpyoy_WI as dUmpyoy, 'WI' as Region from BMO_Macro_m union
select YrM,Ump_IN as Ump,dUmp_IN as [dUmp],dUmpyoy_IN as dUmpyoy, 'IN' as Region from BMO_Macro_m 
)a
left join BMO_YrM b on a.YrM = b.YrM
order by Region,a.YrM 

---Table: Dynamic Macro---
If Object_id('[ResearchDev].dbo.BMO_DY_Macro') is not null drop table [ResearchDev].dbo.BMO_DY_Macro
select 
dy.*
,LoanAge = (floor(cast(dy.YYYY_MM as int)/100.0)-year(st.OriginationDate))*12+cast(dy.YYYY_MM as int)%100-month(st.OriginationDate)
,st.LienPosition	
,st.c_interest_only	
,st.Property	
,st.Occupancy	
,st.Purpose	
,st.RateType	
,st.FixedRatePeriod	
,st.Documentation	
,st.OriginalAppraisalAmount	
,st.LTV	
,st.JrLTV	
,st.CombinedLTV	
,st.OriginalAmount	
,st.Srbalance	
,st.OriginalTerm	
,st.AmorTerm	
,st.IOTerm	
,st.State	
,st.Zip	
,st.FICO	
,st.ORIG_INT_RT	
,st.OriginationDate	
,st.PrepayPenaltyTerm	
,st.MaxDrawnAmount	
,st.DrawdownTerm	
,st.DTI	
,st.IsModified	
,st.IsBroker	
,st.cntrct_cd	
,st.HPI_Region
--,st.isHELOC
--,st.isHarris
,year(st.OriginationDate)*100 + month(st.OriginationDate) as YM_orig
,Umpt.Ump
,Umpt.[dUmp]
,Umpt.dUmpyoy
,Ump1.Ump as Ump_L1
,Ump1.[dUmp] as dUmp_L1
,vUmp = Umpt.Ump - Ump0.Ump
,mt.CPI
,mt.gCPI
,m1.gCPI as [gCPI_L1]
,mt.gCPIyoy
,vCPI = log(mt.CPI) - log(m0.CPI)
,mt.Fed_Funds_Target_Rate
,m1.Fed_Funds_Target_Rate as Fed_Funds_Target_Rate_L1
,m0.Fed_Funds_Target_Rate as Fed_Funds_Target_Rate_orig
,mt.TedSpread
,m1.TedSpread as TedSpread_L1
,m0.TedSpread as TedSpread_orig
,mt.Libor_6M
,m1.Libor_6M as Libor_6M_L1
,m0.Libor_6M as Libor_6M_orig
,mt.MortgageRate_30Y
,m1.MortgageRate_30Y as MortgageRate_30Y_L1
,m0.MortgageRate_30Y as MortgageRate_30Y_orig
,vMortgageRate_30Y = mt.MortgageRate_30Y - m0.MortgageRate_30Y
,mt.MortgageRate_15Y
,m1.MortgageRate_15Y as MortgageRate_15Y_L1
,m0.MortgageRate_15Y as MortgageRate_15Y_orig
,vMortgageRate_15Y = mt.MortgageRate_15Y - m0.MortgageRate_15Y
,mt.SwapRate_10Y
,m1.SwapRate_10Y as SwapRate_10Y_L1
,m0.SwapRate_10Y as SwapRate_10Y_orig
,vSwapRate_10Y = mt.SwapRate_10Y- m0.SwapRate_10Y

,mt.ARM_1Y
,m1.ARM_1Y as ARM_1Y_L1
,m0.ARM_1Y as ARM_1Y_orig
,vARM_1Y = mt.ARM_1Y - m0.ARM_1Y
,mt.ARM_5Y
,m1.ARM_5Y as ARM_5Y_L1
,m0.ARM_5Y as ARM_5Y_orig
,vARM_5Y = mt.ARM_5Y - m0.ARM_5Y
,hpit.HPI
,hpit.gHPI
,hpi1.gHPI as gHPI_L1
,hpit.gHPIyoy
,vHPI = log(hpit.HPI) - log(hpi0.HPI)
into ResearchDev.dbo.BMO_DY_Macro
from BMO_ALL_DY_Clean dy
left join BMO_YrM dt on 
dy.YYYY_MM = dt.YrM
left join BMO_ALL_ST_Clean st on 
dy.loanid = st.loanid
left join BMO_MACRO_M mt
on dy.YYYY_MM = mt.YrM
left join BMO_MACRO_M m1
on dt.t = m1.t + 1
left join BMO_MACRO_M m0
on year(st.OriginationDate)*100 + month(st.OriginationDate) = m0.YrM
left join BMO_HPI hpit
on dy.YYYY_MM = hpit.YrM and st.HPI_Region COLLATE DATABASE_DEFAULT = hpit.Region
left join BMO_HPI hpi1
on dt.t = hpi1.t + 1 and st.HPI_Region COLLATE DATABASE_DEFAULT = hpi1.Region
left join BMO_HPI hpi0
on year(st.OriginationDate)*100 + month(st.OriginationDate) =  hpi0.YrM and st.HPI_Region COLLATE DATABASE_DEFAULT = hpi0.Region

left join BMO_Ump Umpt
on dy.YYYY_MM = Umpt.YrM and st.Ump_Region COLLATE DATABASE_DEFAULT = Umpt.Region
left join BMO_Ump Ump1
on dt.t = Ump1.t + 1 and st.Ump_Region COLLATE DATABASE_DEFAULT = Ump1.Region
left join BMO_Ump Ump0
on year(st.OriginationDate)*100 + month(st.OriginationDate) =  Ump0.YrM and st.Ump_Region COLLATE DATABASE_DEFAULT = Ump0.Region
--rth
--order by YYYY_MM




If Object_id('[ResearchDev].dbo.BMO_LGD_Macro') is not null drop table [ResearchDev].dbo.BMO_LGD_Macro
select 
lgd.*
,LoanAge = (floor(cast(lgd.Def_YM as int)/100.0)-year(st.OriginationDate))*12+cast(lgd.Def_YM as int)%100-month(st.OriginationDate)
,st.LienPosition	
,st.c_interest_only	
,st.Property	
,st.Occupancy	
,st.Purpose	
,st.RateType	
,st.FixedRatePeriod	
,st.Documentation	
,st.OriginalAppraisalAmount	
,st.LTV	
,st.JrLTV	
,st.CombinedLTV	
,st.OriginalAmount	
,st.Srbalance	
,st.OriginalTerm	
,st.AmorTerm	
,st.IOTerm	
,st.State	
,st.Zip	
,st.FICO	
,st.ORIG_INT_RT	
,st.OriginationDate	
,st.PrepayPenaltyTerm	
,st.MaxDrawnAmount	
,st.DrawdownTerm	
,st.DTI	
,st.IsModified	
,st.IsBroker	
,st.cntrct_cd	
,st.HPI_Region
,st.isHELOC
,st.isHarris
,year(st.OriginationDate)*100 + month(st.OriginationDate) as YM_orig
,Umpt.Ump
,Umpt.[dUmp]
,Umpt.dUmpyoy
,Ump1.Ump as Ump_L1
,Ump1.[dUmp] as dUmp_L1
,vUmp = Umpt.Ump - Ump0.Ump
,mt.CPI
,mt.gCPI
,m1.gCPI as [gCPI_L1]
,mt.gCPIyoy
,vCPI = log(mt.CPI) - log(m0.CPI)
,mt.Fed_Funds_Target_Rate
,m1.Fed_Funds_Target_Rate as Fed_Funds_Target_Rate_L1
,m0.Fed_Funds_Target_Rate as Fed_Funds_Target_Rate_orig
,mt.TedSpread
,m1.TedSpread as TedSpread_L1
,m0.TedSpread as TedSpread_orig
,mt.Libor_6M
,m1.Libor_6M as Libor_6M_L1
,m0.Libor_6M as Libor_6M_orig
,mt.MortgageRate_30Y
,m1.MortgageRate_30Y as MortgageRate_30Y_L1
,m0.MortgageRate_30Y as MortgageRate_30Y_orig
,vMortgageRate_30Y = mt.MortgageRate_30Y - m0.MortgageRate_30Y
,hpit.HPI
,hpit.gHPI
,hpi1.gHPI as gHPI_L1
,hpit.gHPIyoy
,vHPI = log(hpit.HPI) - log(hpi0.HPI)
into [ResearchDev].dbo.BMO_LGD_Macro
from BMO_ALL_LGD lgd 
left join BMO_YrM dt on 
lgd.Def_YM = dt.YrM
left join BMO_ALL_ST_Clean st on 
lgd.loanid = st.loanid
left join BMO_MACRO_M mt
on lgd.Def_YM = mt.YrM
left join BMO_MACRO_M m1
on dt.t = m1.t + 1
left join BMO_MACRO_M m0
on year(st.OriginationDate)*100 + month(st.OriginationDate) = m0.YrM
left join BMO_HPI hpit
on lgd.Def_YM = hpit.YrM and st.HPI_Region COLLATE DATABASE_DEFAULT = hpit.Region
left join BMO_HPI hpi1
on dt.t = hpi1.t + 1 and st.HPI_Region COLLATE DATABASE_DEFAULT = hpi1.Region
left join BMO_HPI hpi0
on year(st.OriginationDate)*100 + month(st.OriginationDate) =  hpi0.YrM and st.HPI_Region COLLATE DATABASE_DEFAULT = hpi0.Region
left join BMO_Ump Umpt
on lgd.Def_YM = Umpt.YrM and st.Ump_Region COLLATE DATABASE_DEFAULT = Umpt.Region
left join BMO_Ump Ump1
on dt.t = Ump1.t + 1 and st.Ump_Region COLLATE DATABASE_DEFAULT = Ump1.Region
left join BMO_Ump Ump0
on year(st.OriginationDate)*100 + month(st.OriginationDate) =  Ump0.YrM and st.Ump_Region COLLATE DATABASE_DEFAULT = Ump0.Region

CREATE INDEX PIndex
ON [ResearchDev].dbo.BMO_LGD_MACRO (LoanID)



