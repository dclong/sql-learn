
drop table user_work.A;
create  table user_work.A
(id integer)
;

insert into User_Work.A values(1);
insert into User_Work.A values(2);
insert into User_Work.A values(3);
insert into User_Work.A values(4);
insert into User_Work.A values(5);
insert into User_Work.A values(6);
insert into User_Work.A values(7);
insert into User_Work.A values(8);
insert into User_Work.A values(9);
insert into User_Work.A values(10);
insert into User_Work.A values(11);
insert into User_Work.A values(12);
insert into User_Work.A values(13);
insert into User_Work.A values(14);
insert into User_Work.A values(15);

drop table user_work.B;
create table user_work.B (
    id integer
)
;
insert into User_Work.B values(1);
insert into User_Work.B values(2);
insert into User_Work.B values(3);
insert into User_Work.B values(4);
insert into User_Work.B values(5);
insert into User_Work.B values(6);
insert into User_Work.B values(7);
insert into User_Work.B values(8);
insert into User_Work.B values(9);
insert into User_Work.B values(10);
insert into User_Work.B values(12);
insert into User_Work.B values(15);

select 
    count(*) as cnt 
from 
    user_work.A A 
inner join 
    user_work.B B 
on 
    A.id = B.id
;

select 
    count(*) as cnt 
from 
    user_work.A A 
left join 
    user_work.B B 
on 
    A.id = B.id
;


select 
    a.id as aid,
    b.id as bid
from 
    user_work.A A 
join 
    user_work.B B 
on 
    A.id = B.id
;

select 
    count(distinct B.id) as cnt 
from 
    user_work.A A 
left join 
    user_work.B B 
on 
    A.id = B.id
;

select 
    count(*) as cnt 
from 
    user_work.A A 
left join 
    user_work.B B 
on 
    A.id = B.id 
and 
    B.id > 10
;

select 
    *
from 
    user_work.A A 
left join 
    user_work.B B 
on 
    A.id = B.id 
and 
    B.id > 10
;

select 
    count(*) as cnt 
from 
    user_work.A A 
left join 
    user_work.B B 
on 
    A.id = B.id 
and 
    A.id > 10
;

select 
    *
from 
    user_work.A A 
left join 
    user_work.B B 
on 
    A.id = B.id 
and 
    A.id > 10
;


select 
    count(distinct B.id)  
from 
    user_work.A A 
left join 
    user_work.B B 
on 
    A.id = B.id 
and 
    B.id > 10
;


select 
    count(distinct a.id)  
from 
    user_work.A A 
left join 
    user_work.B B 
on 
    A.id = B.id 
where 
    a.id > 10
;

select 
    count(*)  
from 
    user_work.A A 
left join 
    user_work.B B 
on 
    A.id = B.id 
where 
    b.id > 10
;
