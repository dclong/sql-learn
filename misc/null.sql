
drop table user_work.testnull;
create table user_work.testnull (
    id integer, 
    sar integer
)
;
insert into 
    user_work.testnull 
values
    (1, 1)
;
insert into 
    user_work.testnull 
values
    (1, 0)
;
insert into 
    user_work.testnull 
values
    (2, 1)
;
insert into 
    user_work.testnull 
values
    (3, 0)
;
insert into 
    user_work.testnull 
values
    (null, 1)
;
insert into 
    user_work.testnull 
values
    (null, 0)
;
/*
what was your conclusion about null values?
*/


